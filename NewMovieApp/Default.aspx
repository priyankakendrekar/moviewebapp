﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NewMovieApp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <div class="col-lg-8">  
    <div class="panel panel-primary">  
        <div class="panel-heading">  
            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> MovieDetails </h3>  
        </div>  
        <div class="panel-body">  
            <div class="form-group col-lg-12">  
                <label>Title</label>  
                <input type="text" name="Title" id="txtTitle" class="form-control" placeholder="Title" required="" />  
            </div>  
            
            <div class="clearfix"></div>  
            <div class="form-group col-lg-12">  
                <label>Director</label>  
                <input type="text" name="Director" id="txtDirector" class="form-control" placeholder="Director" required="" />  
            </div>  
            <div class="form-group col-lg-12">  
                <label>Release Date </label>  
                <input type="date" name="RelDate" id="relDate" min="1920-01-02" class="form-control datepicker" required="" />  
            </div>  
            <div class="form-group col-lg-6">  
                <label>MovieGenre</label>  
                <select name="MovieGenre" id="MovieGenre" class="form-control" required="">  
                    <option value="" disabled="disabled">-- Select -- </option>  
                    <option value="Comedy">Comedy</option>  
                    <option value="Horror">Horror</option>
                    <option value="Romantic">Romantic</option>
                    <option value="SciFic">SciFic</option>
                    
                </select>  
            </div>  
             
              <div class="form-group col-lg-12">  
                <label>Casts</label>  
                <textarea rows="2" name="Casts" id="Casts" class="form-control" required="">  </textarea>
                    
                    </div>  
                     
                      
                    <div class="form-group col-lg-8">  
                        <div style="float: right">  
                            <input value="Cancel" id="BtnCancel" class="btn btn-primary" type="button">  
                            <input class="btn btn-primary" name="submitButton" id="btnSave" value="Save" type="button">  
                        </div>  
                    </div>  
            </div>  
       
    </div>  
</div>  
 

            <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/json2/0.1/json2.js"></script>
    <script type="text/javascript">  
    $(function() {  
        $("#btnSave").click(function () {
            var movie = {};
            movie.Title = $("#txtTitle").val(); 
            movie.Director = $("#txtDirector").val();
            movie.RelDate = $("#relDate").val();
            movie.MovieGenre = $("#MovieGenre").val();  
            movie.Casts = $("#Casts").val();  
             
            $.ajax({  
                type: "POST",  
                url: "Default.aspx/SaveMovie", 
                data: '{objMovie: ' + JSON.stringify(movie) + '}', 
                dataType: "json",  
                contentType: "application/json; charset=utf-8",  
                success: function() {  
                    alert("User has been added successfully.");  
                    
                },  
                error: function() {  
                    alert("Error while inserting data");  
                }  
            });  
            return false;  
        });  
    });  
</script> 

  

</asp:Content>
