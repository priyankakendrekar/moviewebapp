﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NewMovieApp
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
       
        }

        public class Movies
        {
            public int MovieId;
            public string Title;
            public string Director;
            public string MovieGenre;
            public string Casts;
            public DateTime RelDate;

        }
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public static void SaveMovie(Movies objMovie) //Insert data in database  
        {
            
            using (var con = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=MovieDB;Integrated Security=True"))
            {
                using (var cmd = new SqlCommand("INSERT INTO TblMovies VALUES(@Title, @Director,@RelDate,@MovieGenre,@Casts)"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Parameters.AddWithValue("@Title", objMovie.Title);
                    cmd.Parameters.AddWithValue("@Director", objMovie.Director);
                    cmd.Parameters.AddWithValue("@RelDate", objMovie.RelDate);
                    cmd.Parameters.AddWithValue("@MovieGenre", objMovie.MovieGenre);
                    cmd.Parameters.AddWithValue("@Casts", objMovie.Casts);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            
        }
    }
    }
